package javaTetris;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;

public class MenuButton extends JButton {

	private String name;
	private String btAction;
	private Font font=new Font("Ink Free", Font.ITALIC, 25);
	private FontMetrics fm=getFontMetrics(font);
	private Image img;
	private Color color=Color.WHITE;
	
//variable test audio

private File sonVal=new File("audio/valid.mp3");
//____________
	
	
	
	public MenuButton(String txt, String action) {
		super(txt);
		this.name=txt;
		this.btAction=action;
		try {
			img=ImageIO.read(new File("img/bgBtn1.png"));
		}catch(IOException e){
			e.printStackTrace();
		}
		this.setBorderPainted(false);
		this.setPreferredSize(new Dimension(250,65));
		this.addMouseListener(new test(this));
		
	}
	
	public void paintComponent(Graphics g) {
		g.setColor(new Color(0,0,0,0));
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		
		g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		
		g.setColor(color);
		g.setFont(font);
		g.drawString(this.name, (this.getWidth()/2)-(fm.stringWidth(this.name)/2), (this.getHeight()/2)+12);
		}

	//class interne
	class test implements MouseListener{

		private MenuButton aze;
		
		public test(MenuButton aze) {
			this.aze=aze;
		}
		
		@Override
		public void mouseClicked(MouseEvent e) {
			
		}


		public void mousePressed(MouseEvent e) {
			color=Color.RED;
			aze.resize(new Dimension(245,60));
			SoundThread test=new SoundThread("valid");
			test.start();
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			if(e.getY()>0 && e.getY()<aze.getHeight() && e.getX() >0 && e.getX() < aze.getWidth())
				color=Color.GREEN;
			aze.resize(new Dimension(250,65));
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			color=Color.GREEN;
			SoundThread test=new SoundThread("choose");
			test.start();
		}

		@Override
		public void mouseExited(MouseEvent e) {
			color=Color.WHITE;
			aze.resize(new Dimension(250,65));
		}
			
	}
	
	
	
	
	
	//getteur setteurs
	public String getBtAction() {return this.btAction;}
	
}
