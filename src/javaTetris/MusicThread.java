package javaTetris;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

public class MusicThread extends Thread{
	private File soundFile;
	
	public MusicThread(String sound) {
		soundFile=new File("audio/music/"+sound+".mp3");
	}
	
	public void run() {
		try {
			Player aze=new Player(new FileInputStream(soundFile));
			aze.play();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JavaLayerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	
	
}
