package javaTetris;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class GamePan extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	//variable d'affichage
	private Image img;
	private Image imgPause;
	private Image[] tetrImg=new Image[7];
	private ArrayList<ArrayList<Integer>> tetrisGrid = new ArrayList<ArrayList<Integer>>();
	private int sizeElm;
	private InfoPan RightPan;
	private ScorePan LeftPan;
	//vatiable de jeu
	private boolean game=false;
	private int decompte=300;
	private int[]dataTetrino;
	private int tetrino[][];
	private int positionX;
	private int positionY;
	//variable d'options
	private int shadow=0; 
	private int column=0; 
	//music
	private Thread music;
	
	private boolean pause=false;
	
	//constructeur
	public GamePan(int width, int height, InfoPan RightPan, ScorePan LeftPan) {
		

		sizeElm=(width/12);
		try {
			for(int i=0;i<7;i++) {
				String filePath="img/tetrino/tetri"+String.valueOf(i+1)+"elm.png";
				tetrImg[i]=ImageIO.read(new File(filePath));
			}
		}catch(IOException e) {
			e.printStackTrace();
		}
		System.out.println("init de la grille");
		for(int i=0;i<22;i++) {
			ArrayList<Integer> line=new ArrayList<Integer>();
			for(int j=0;j<10;j++) {
				line.add(0);
			}
			tetrisGrid.add(line);
		}
		System.out.println("chargement de l'image de fond de la grille");	
		try {
			img=ImageIO.read(new File("img/bgGame1.png"));
		}catch(IOException e) {
			e.printStackTrace();
		}
		System.out.println("chargement de l'image de fond de la pause");	
		try {
			imgPause=ImageIO.read(new File("img/pause.png"));
		}catch(IOException e) {
			e.printStackTrace();
		}
		this.setPreferredSize(new Dimension(width,height));
		this.setBackground(new Color(0,0,0,0));
		this.RightPan=RightPan;
		this.LeftPan=LeftPan;
		
		new ThreadDecompte(this).start();
	}
	
	//dessin du paneau
	public void paintComponent(Graphics g) {
		if(this.pause) {
			g.fillRect(0, 0, this.getWidth(), this.getHeight());
			g.drawImage(imgPause,0,0,this.getWidth(),this.getHeight(), this);
		}
		else{
			g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);	
			//afficher la grille
			
			for(int i=0;i<tetrisGrid.size();i++) {
				for(int j=0;j<tetrisGrid.get(i).size();j++) {
					//debug
					if(Config.debug) {
						g.setColor(new Color(250,50,20,250));
						g.drawRect(25+(sizeElm*j), 20+(sizeElm*i), sizeElm,sizeElm);
						g.drawString(String.valueOf(this.tetrisGrid.get(i).get(j)), 40+(sizeElm*j), 40+(sizeElm*i));
					}
					
					
					//afficher grille
					if(tetrisGrid.get(i).get(j)==0 && Config.showGrid) {
						g.setColor(new Color(55,230,20,50));
						g.drawRect(25+(sizeElm*j), 20+(sizeElm*i), sizeElm,sizeElm);
					}
						
					//afficher colone
					if(tetrisGrid.get(i).get(j)<0 && Config.showColumn) {
						g.setColor(new Color(100,130,202,60));
						g.fillRect(25+(sizeElm*j), 20+(sizeElm*i), sizeElm,sizeElm);
						if(Config.showGrid) {
							g.setColor(new Color(55,230,20,50));
							g.drawRect(25+(sizeElm*j), 20+(sizeElm*i), sizeElm,sizeElm);
					
						}
					}
					//afficher ombre
					if(tetrisGrid.get(i).get(j)==-2 && Config.showShadow) {
						g.setColor(new Color(100,130,202,160));
						g.fillRect(25+(sizeElm*j), 20+(sizeElm*i), sizeElm,sizeElm);
						if(Config.showGrid) {
							g.setColor(new Color(55,230,20,50));
							g.drawRect(25+(sizeElm*j), 20+(sizeElm*i), sizeElm,sizeElm);
						}
					}
					
					//afficher le tetrino sur la grille
					if(tetrisGrid.get(i).get(j)>0 && tetrisGrid.get(i).get(j)<8)
						g.drawImage(tetrImg[tetrisGrid.get(i).get(j)-1], 25+(sizeElm*j), 20+(sizeElm*i), sizeElm,sizeElm, this);
					//si ligne
					
					if(tetrisGrid.get(i).get(j)==10)
					{
						g.setColor(Color.white);
						g.fillRect(25+(sizeElm*j), 20+(sizeElm*i), sizeElm,sizeElm);
					}
					if(tetrisGrid.get(i).get(j)==11)
					{
						g.setColor(Color.gray);
						g.fillRect(25+(sizeElm*j), 20+(sizeElm*i), sizeElm,sizeElm);
					}
					
					
					
					
					
					//si perdu 
					if(tetrisGrid.get(i).get(j)==-3) {
						g.setColor(new Color(150,150,150,250));
						g.fillRect(25+(sizeElm*j), 20+(sizeElm*i), sizeElm,sizeElm);
						g.setColor(Color.black);
						g.drawRect(25+(sizeElm*j), 20+(sizeElm*i), sizeElm,sizeElm);
					}
					
				}
			}		
			//decompte
			if(game==false && decompte>-1) {
				g.setFont(new Font("Ink Free", Font.BOLD,35));
				g.setColor(Color.WHITE);
				//affiche le cercle complet
				Graphics2D g2d=(Graphics2D)g;
				BasicStroke line = new BasicStroke(6.0f);
				g2d.setStroke(line);
				if(decompte>200) {
					g2d.setColor(new Color(60,100,230,200));
					g.drawString("3", (this.getWidth()/2)-15, (this.getHeight()/2)+10);
					g2d.drawArc((this.getWidth()/2)-50, (this.getHeight()/2)-50, 100, 100, 0, (320-decompte)*(360/100));
				}
				else if(decompte>100) {
					g2d.setColor(new Color(230,60,100,200));
					g.drawString("2", (this.getWidth()/2)-15, (this.getHeight()/2)+10);
					g2d.drawArc((this.getWidth()/2)-50, (this.getHeight()/2)-50, 100, 100, 0, (220-decompte)*(360/100));
				}
				else if(decompte>0) {
					g2d.setColor(new Color(100,230,60,200));
					g.drawString("1", (this.getWidth()/2)-15, (this.getHeight()/2)+10);
					g2d.drawArc((this.getWidth()/2)-50, (this.getHeight()/2)-50, 100, 100, 0, (120-decompte)*(360/100));
				}
				else {
					g2d.setColor(new Color(255,255,255,200));
					g.drawString("GO", (this.getWidth()/2)-25, (this.getHeight()/2)+10);
				}
			}
		}
	}
	

	//initialisation du jeu
	public void init() {
		this.game=true;
		this.dataTetrino=Tetrinos.GenTetrino();
		this.tetrino=Tetrinos.getTetrino(dataTetrino[0],dataTetrino[1]);
		
		
		this.positionX=5;
		this.positionY=0;
		new MusicloopThread(this,Config.time[Config.musicSelect],Config.track[Config.musicSelect]).start();
//		new SoundThread("theme").start();
		this.startGame();
	}
	
	public void startGame() {
		this.game=true;
		new GameTHread(this).start();
	}

	//peindre le tetrino sur la grille
	public void paintTetrino() {
		for(int i=0; i<this.tetrisGrid.size();i++)
			for(int j=0;j<this.tetrisGrid.get(i).size();j++)
				if(this.tetrisGrid.get(i).get(j)<0)this.tetrisGrid.get(i).set(j, 0);
					
		
		
		for(int i=0;i<4;i++) {
			this.setTetrisGrig(this.positionX+tetrino[i][0], this.positionY+tetrino[i][1], tetrino[8][0]);

			//si option shadow
			if(Config.showShadow) {
				
				if(this.checkDown()) {
					this.shadow=positionY+1;
					while(this.checkShadow()) {
						this.shadow++;
					}	
					
					if(this.tetrisGrid.get(this.shadow+tetrino[i][1]).get(this.positionX+tetrino[i][0])==0)
						this.setTetrisGrig(this.positionX+tetrino[i][0], this.shadow+tetrino[i][1],-2);	
				}
			}
		}
		//si option colone
		if(Config.showColumn) {
			for(int elm:this.tetrino[4]) {
				this.column=positionY+1;
				
					while(this.tetrino[elm][1]+column<22&&this.tetrisGrid.get(this.tetrino[elm][1]+column).get(this.tetrino[elm][0]+this.positionX)==0) {
						this.tetrisGrid.get(this.tetrino[elm][1]+column).set(this.tetrino[elm][0]+this.positionX,-1);
						column++;
					}
			
			
			}	
		}
		this.repaint();
	}
	//effacer le tetrino sur la grille
	public void delTetrino() {
		for(int i=0;i<4;i++) {
			this.setTetrisGrig(this.positionX+tetrino[i][0], this.positionY+tetrino[i][1], 0);
		}
	}
	//verification de la hauteur
	public void checkTop() {
		if(this.positionY-this.tetrino[7][0]<0)	
			this.positionY=this.tetrino[7][0];
	}
	
	//descendre
	public void goDown() {
		if(this.checkDown()) {
			this.delTetrino();
			this.positionY++;
			this.paintTetrino();					
		}
	}
	//verifi descente
	public boolean checkDown() {
		for(int elm:tetrino[4]) {
			if(this.positionY+this.tetrino[elm][1]+1>=22)
				return false;
			if(this.tetrisGrid.get(this.positionY+this.tetrino[elm][1]+1).get(this.positionX+this.tetrino[elm][0])>0)
				return false;
		}
		return true;
	}
	//aller a gauche
	public void goLeft() {
		if(this.checkLeft()) {
			this.delTetrino();
			this.positionX--;
			this.paintTetrino();
		}
	}
	//verifi la gauche
	public boolean checkLeft() {
		for(int elm:tetrino[5]) {
			if(this.positionX+this.tetrino[elm][0]-1<0)
				return false;
			if(this.tetrisGrid.get(this.positionY+this.tetrino[elm][1]).get(this.positionX+this.tetrino[elm][0]-1)>0)
				return false;
		}
		return true;
	}
	//aller a droite
	public void goRight() {
		if(this.checkRight()) {
			this.delTetrino();
			this.positionX++;
			this.paintTetrino();
		}
	}
	//verifi la droite
	public boolean checkRight() {
		for(int elm:tetrino[6]) {
			if(this.positionX+this.tetrino[elm][0]+1>=10)
				return false;
			if(this.tetrisGrid.get(this.positionY+this.tetrino[elm][1]).get(this.positionX+this.tetrino[elm][0]+1)>0)
				return false;
		}
		return true;
	}

	//verification et rotation de la forme
	public void rotation(boolean horaire){
			//forme temporaire
			int tempForme[][];
			int tempRot;
			if(horaire==true) {
				if(this.dataTetrino[1]<Tetrinos.getLastRot(this.dataTetrino[0])) {
					tempRot= this.dataTetrino[1]+1;
				}
				else {
					tempRot=0;
				}
			}else {
				if(this.dataTetrino[1]>0) {
					tempRot= this.dataTetrino[1]-1;
				}
				else {
					tempRot=Tetrinos.getLastRot(this.dataTetrino[0]);
				}
			}
			tempForme= Tetrinos.getTetrino(dataTetrino[0], tempRot);
//			si la forme temporaire sort de la grille
			int tempX=positionX;
			int tempY=positionY;
			for(int i=0;i<4;i++) {
			while(tempX+tempForme[i][0]<0)tempX++;
			while(tempX+tempForme[i][0]>this.tetrisGrid.get(0).size()-1) tempX--;
			while(tempY+tempForme[i][1]<0)tempY++;
			while(tempY+tempForme[i][1]>this.tetrisGrid.size()-1) tempY--;
			}
		// verif si rotation possible
			boolean makeRot=true;
			this.delTetrino();
			for(int i=0;i<4;i++) {
			if(this.tetrisGrid.get(tempY+tempForme[i][1]).get(tempX+tempForme[i][0])>0)
				makeRot=false;
			}
			this.paintTetrino();
			//pivote la piece
			if (makeRot==true) {
				this.delTetrino();
				this.tetrino=tempForme;
				positionX=tempX;
				positionY=tempY;
				this.dataTetrino[1]=tempRot;
				this.paintTetrino();
		}
			
	}
	//verification de ligne
	private ArrayList<Integer> checkLine() {
		ArrayList<Integer> toDel = new ArrayList<Integer>();
		for(int i=0;i<this.tetrisGrid.size();i++) {
			boolean  line=true;
			for(int value:this.tetrisGrid.get(i)) {
				if(value<=0) line=false;
			}
			if(line) toDel.add(i);
		}
		return toDel;
	}
	//suppression des lignes
			public void delLine() {
				ArrayList<Integer> toRemove=this.checkLine();
				if(toRemove.size()>0) {
					System.out.println("nbre de ligne : "+toRemove.size());
					new SoundThread("line").start();
					if(toRemove.size()==4)new SoundThread("awesome").start();
					// AnimLineRemove
					
					
					
					for(int j=0;j<8;j++) {
						
						
						for(int lineToRemove:toRemove) {
							for(int i=0;i<this.tetrisGrid.get(lineToRemove).size();i++) {
								if(this.tetrisGrid.get(lineToRemove).get(i)==10)
									this.tetrisGrid.get(lineToRemove).set(i, 11);
								else this.tetrisGrid.get(lineToRemove).set(i, 10);	
							}
						}
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.out.println("test");
						this.repaint();
						
					}
					
					

					
					
					
					
					
					for(int lineToRemove:toRemove) {
						this.tetrisGrid.remove(lineToRemove);
						ArrayList<Integer> newLine=new ArrayList<Integer>();
						for(int j=0;j<10;j++)
							newLine.add(0);
						//ajouter le nbre de ligne
						this.tetrisGrid.add(0, newLine);
					}
					//si lvlUp
					if(this.getLeftPan().addLine(toRemove.size())) 
					{
						System.out.println("level up");
						//TODO JOUER SON LEVELUP	
						
					}
					//ajouter les pts
					this.RightPan.addScore(toRemove.size(),this.getLeftPan().getLevel());
				}
			}
	
	//perdu!
	public void loose() {
		for(int i=this.tetrisGrid.size()-1; i>=0;i--) {
			for(int j=0;j<this.tetrisGrid.get(i).size();j++) {
				this.tetrisGrid.get(i).set(j, -3);
			}
			this.repaint();
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	

	//pause
	public void pause() {
		if(this.game){
			this.setGame(false);
			this.pause=true;
			this.repaint();
			System.out.println("jeu en pause");

		}
		else {
			this.pause=false;
			this.repaint();
			this.startGame();
			System.out.println("reprise du jeu");

		}
			
	}
	//decompte
	public void decompte() {
		new ThreadDecompte(this).start();
	}
	
	// ombre
	public boolean checkShadow() {
		for(int elm:tetrino[4]) {
			if(this.shadow+this.tetrino[elm][1]+1>=22)
				return false;
			if(this.tetrisGrid.get(this.shadow+this.tetrino[elm][1]+1).get(this.positionX+this.tetrino[elm][0])>0)
				return false;
		}
		return true;
	}
	
	//getteur setteur
	
	public boolean getGame() {return this.game;}
	public int getDecompte() {return this.decompte;}
	public int[][] getTetrino(){return this.tetrino;}
	public int[] getDataTetrino() {return this.dataTetrino;}
	public int getPositionX() {return this.positionX;}
	public int getPositionY() {return this.positionY;}
	public ArrayList<ArrayList<Integer>> getTetrisGrid() {return this.tetrisGrid;}
	public InfoPan getRightPan() {return this.RightPan;}
	public ScorePan getLeftPan() {return this.LeftPan;}
	public boolean getPause() {return this.pause;}
	public Thread getMusic() {return this.music;}
	
	public void setGame(boolean bool) {this.game=bool;}
	public void setDecompte(int decompte) {this.decompte=decompte;}
	public void setTetrino(int[][] tetrino) {this.tetrino=tetrino;}
	public void setDataTetrino(int[] dataTetrino) {this.dataTetrino=dataTetrino;}

	public void setPositionX(int pX) {this.positionX=pX;}
	public void setPositionY(int pY) {this.positionY=pY;}
	public void setTetrisGrig(int pX, int pY, int elm) {this.tetrisGrid.get(pY).set(pX, elm);}
	public void setMusic(Thread music) {this.music=music;}
	




}	
