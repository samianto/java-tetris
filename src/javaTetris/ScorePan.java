package javaTetris;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class ScorePan extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Image img;
	private int nbLine=0;
	private int level=1;
	private int lineToLvl=10;
	
	public ScorePan(int width, int height) {
		System.out.println("chargement de l'image de fond du panneau Score");	
		try {
			img=ImageIO.read(new File("img/gauche.png"));
		}catch(IOException e) {
			e.printStackTrace();
		}
		this.setPreferredSize(new Dimension(width,height));
		this.setBackground(new Color(0,0,0,0));
	
	}
	
	public void paintComponent(Graphics g) {
		
		g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);	
		g.setFont(new Font("Ink Free", Font.BOLD,35));
		g.setColor(Color.WHITE);
		if(nbLine<2) g.drawString("Ligne", 55, 75);
		else g.drawString("Lignes", 55, 75);
		g.drawString(String.valueOf(nbLine), 60, 130);
		g.drawString("Niveau", 45, 600);
		
		//affiche le cercle complet
		Graphics2D g2d=(Graphics2D)g;
		BasicStroke line = new BasicStroke(6.0f);
		g2d.setStroke(line);
		g2d.setColor(new Color(60,100,230,200));
		g2d.drawArc(50, 610, 100, 100, 0, 360);
		//affiche le cercle en cour
		g2d.setColor(new Color(0,255,255,255));
		g2d.drawArc(50, 610, 100, 100, 0,(10-lineToLvl)*36);
		//affiche le niveau dans le cercle
		g.setColor(Color.WHITE);
		g.drawString(String.valueOf(level), 90, 665);
	}
	//methodes
	public boolean addLine(int line) {
		this.nbLine=this.nbLine+line;
		this.lineToLvl=this.lineToLvl-line;
		if(this.lineToLvl<=0) this.lineToLvl=10-this.lineToLvl;
		System.out.println("nombre de ligne avant levelup : "+this.lineToLvl);
		this.repaint();
		if(this.checkLvl()) return true;
		return false;
	}
	
	private boolean checkLvl() {
		
	while((this.nbLine+10)/10>this.level){
		this.level++;
		this.repaint();
		return true;
	}	
	return false;
	}

	//getteur Setter
	public int getNbLine() {return this.nbLine;}
	public int getLevel() {return this.level;}
	public int getLineToLvl() {return this.lineToLvl;}
	
	public void setNbLine(int nbLine) {this.nbLine=nbLine;}
	public void setLevel(int level) {this.level=level;}
	public void setLineToLvl(int lineToLvl) {this.lineToLvl=lineToLvl;}




}
