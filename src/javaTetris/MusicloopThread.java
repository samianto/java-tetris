package javaTetris;

public class MusicloopThread extends Thread {
	
	private GamePan mainPan;
	private int time;
	private String track;
	private Thread music;
	
	public MusicloopThread(GamePan mainPan, int time, String track) {
		this.time=time;
		this.track=track;
		this.mainPan=mainPan;
	}
	
	public void run() {

		while(true) {
			
			mainPan.setMusic(new MusicThread(track));

			mainPan.getMusic().start();
			try {
				Thread.sleep(time*1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
