package javaTetris;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;

public class GameScreen extends JPanel{

	private ScorePan leftPan;
	private InfoPan rightPan;
	private GamePan mainPan;
	
	public GameScreen(int width, int height) {
		this.setSize(new Dimension(width,height));
		this.setLayout(new BorderLayout());
		leftPan=new ScorePan(200, this.getHeight());
	 	rightPan=new InfoPan(200, this.getHeight());
	 	mainPan=new GamePan(this.getWidth()-400, this.getHeight(), rightPan, leftPan);
	 	this.add(leftPan,BorderLayout.WEST);
	    this.add(rightPan,BorderLayout.EAST);
	    this.add(mainPan,BorderLayout.CENTER);
	}

	public GamePan getMainPan() {return this.mainPan;}
}
