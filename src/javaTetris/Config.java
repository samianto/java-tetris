package javaTetris;

public abstract class Config {

	public static int left=37;
	public static int right=39;
	public static int down=40;
	public static int rot1=32;
	public static int rot2=17;
	public static int pause=80;
	
	public static int musicSelect=1;
	
	public static int[] time={289,292};
	public static String[] track= {"dubStep", "tetris99"};
	
	
	public static int sound=80;
	public static int effect=80;
	public static boolean showGrid=false;
	public static boolean showShadow=true;
	public static boolean showColumn=true;
	
	public static boolean debug=false;
}

