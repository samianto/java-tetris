package javaTetris;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;

public class MenuOption extends JPanel{

	
	private JPanel titre=new JPanel();
	private Image img;
	private Font font=new Font("Ink Free", Font.BOLD, 25);
	private FontMetrics fm=getFontMetrics(font);
	private JPanel options=new JPanel();
	private JPanel control=new JPanel();
	private JPanel optionJeu=new JPanel();
	private MenuButton retour=new MenuButton("Retour", "retour");
	private JComboBox lvlChoise = new JComboBox();
	
	public MenuOption(int width, int height) {
		this.setLayout(new BorderLayout());
		options.setPreferredSize(new Dimension(280,450));
		options.setBackground(new Color(0,0,0,0));
		
		control.setPreferredSize(new Dimension(350,350));
		control.setBackground(new Color(200,200,200,200));
		control.setLayout(new GridLayout(6,2));
	
		control.add(new LabelOption("gauche", 25));
		control.add(new LabelOption("<", 25));
		control.add(new LabelOption("Droite", 25));
		control.add(new LabelOption(">", 25));
		control.add(new LabelOption("Bas", 25));
		control.add(new LabelOption("v", 25));
		control.add(new LabelOption("Rotation horaire", 25));
		control.add(new LabelOption("bas de page", 25));
		control.add(new LabelOption("Rotation anti-horaire", 25));
		control.add(new LabelOption("suppr", 25));
		control.add(new LabelOption("pause", 25));
		control.add(new LabelOption("p", 25));
		

		
		
		
		
		
		
		optionJeu.setPreferredSize(new Dimension(350,350));
		optionJeu.setBackground(new Color(200,200,200,200));
		optionJeu.setLayout(new GridLayout(6,2));
		
		
		optionJeu.add(new LabelOption("musique", 25));
		optionJeu.add(new JCheckBox(""));
		
		optionJeu.add(new LabelOption("bruitage", 25));
		optionJeu.add(new LabelOption("check", 25));
		
		optionJeu.add(new LabelOption("grille visible", 25));
		optionJeu.add(new JCheckBox(""));
		
		optionJeu.add(new LabelOption("afficher l'ombre ", 25));
		optionJeu.add(new JCheckBox(""));
		
		optionJeu.add(new LabelOption("afflcher les lignes", 25));
		optionJeu.add(new JCheckBox(""));
		
		optionJeu.add(new LabelOption("niveau de depart", 25));
		for(int i=1; i<=10;i++) {
			lvlChoise.addItem(i);	
		}
		
		
		
		
		optionJeu.add(lvlChoise);

		options.add(control);
		options.add(optionJeu);
		retour.setFocusable(false);
		options.add(retour);
		
		
		
		
		
		this.add(options, BorderLayout.SOUTH);

		
		
		try{
			img=ImageIO.read(new File("img/bgOption.jpg"));
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		
		
		

	}
	
	public void paintComponent(Graphics g) {
		
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(),this);
		g.setColor(new Color(255,255,255,100));
		g.fillRoundRect((this.getWidth()/2)-150, 50, 300, 60, 50, 50);
		g.setColor(new Color(100,100,255));
		g.setFont(font);
		g.drawString("OPTION", (this.getWidth()/2)-(fm.stringWidth("OPTION")/2), 90);
	
	
	
	
	
	
	}
	public MenuButton getBtnRetour() {return this.retour;}
}
