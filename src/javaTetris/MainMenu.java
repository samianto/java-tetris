package javaTetris;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MainMenu extends JPanel {

	private JLabel jeu=new JLabel("J-Trice");
	private JPanel panImg=new JPanel();
	private JPanel panBtn=new JPanel();
	private MenuButton btns[]= {
			new MenuButton("Jouer", "play"),
			new MenuButton("Option","option"),
			new MenuButton("Mode de jeu", "mode"),
			new MenuButton("Quitter","quit")
	};
	private Image img;
	private Image img2;
	
	public MainMenu(int width, int height){
		this.setPreferredSize(new Dimension(width,height));
		try {
			img=ImageIO.read(new File("img/bgtetris.jpg"));
		}catch(IOException e) {
			e.printStackTrace();
		}
		try {
			img2=ImageIO.read(new File("img/tetris.png"));
		}catch(IOException e) {
			e.printStackTrace();
		}
		panImg.setPreferredSize(new Dimension(width-50, height-400));
		panImg.setBackground(new Color(0,0,0,0));
		
		panBtn.setPreferredSize(new Dimension(300, 300));
		panBtn.setBackground(new Color(0,0,0,0));
		for(MenuButton btn:btns) {
			btn.setFocusable(false);
			panBtn.add(btn);
		}
		
		this.add(panImg);
		this.add(panBtn);
	}
	public void paintComponent(Graphics g) {
		g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		g.drawImage(img2, (panImg.getWidth()/2)-(panImg.getWidth()/4), 10, panImg.getWidth()/2, panImg.getHeight()/1, panImg);
	}	
	
	public MenuButton[] getMainMenuBtns() {return this.btns;}
	
	
	
}
