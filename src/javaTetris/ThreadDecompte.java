package javaTetris;

public class ThreadDecompte extends Thread {
	
	private GamePan mainPan;
	
	public ThreadDecompte(GamePan mainPan) {
		this.mainPan=mainPan;
	}
	
	public void run() {
		for(int i=0;i<300;i++) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			mainPan.setDecompte(mainPan.getDecompte()-1);
			mainPan.repaint();
			if(mainPan.getDecompte()%100==0)
				new SoundThread("choose").start();
		
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		mainPan.setDecompte(mainPan.getDecompte()-1);
		mainPan.repaint();
		mainPan.init();
	}
}
