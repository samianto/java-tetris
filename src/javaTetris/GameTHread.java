package javaTetris;

public class GameTHread extends Thread{
		
	private GamePan mainPan;
	private boolean lose=true;
	public GameTHread(GamePan mainPan) {
		this.mainPan=mainPan;
	}
	
	public void run() {
		//algorythme de vitesse de descente
		int vts=1000-mainPan.getLeftPan().getLevel()*75;
		int algo=vts-vts/5;
		
		
		//lancement de la boucle infini
		while(mainPan.getGame()){
			if(this.mainPan.checkDown())
				this.lose=false;
			
			mainPan.checkTop();
			mainPan.paintTetrino();
			//pause avant tour de bouble
			try {
				Thread.sleep(algo);
			}catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			if(mainPan.checkDown()) {
				mainPan.goDown();
			}else {
				if(lose) {
					System.out.println("perdu");
					this.mainPan.setGame(false);
					this.mainPan.getMusic().stop();
					this.mainPan.loose();
				}
				else {
//					new SoundThread("fall").start();

					//verif si ligne
					mainPan.delLine();
					
					//envoie la preview dans le mainPan
					mainPan.setDataTetrino(mainPan.getRightPan().getDataPreview());
					mainPan.setTetrino(mainPan.getRightPan().getPreview());
					//reset les positions
					mainPan.setPositionX(5);
					mainPan.setPositionY(0);
					//genere letrino suivant
					mainPan.getRightPan().setDataPreview(Tetrinos.GenTetrino());
					mainPan.getRightPan().setPreview(Tetrinos.getTetrino(mainPan.getRightPan().getDataPreview()[0], mainPan.getRightPan().getDataPreview()[1]));
					//repaint la grille
					mainPan.getRightPan().repaint();
				}
				mainPan.setGame(false);
			}

		}
		if(mainPan.getPause()==false&&this.lose==false) mainPan.startGame();
	}

}
