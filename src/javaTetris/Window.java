package javaTetris;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

public class Window extends JFrame implements ActionListener{

	
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private int toLoad=1;
	private Image img;

	
	private GameScreen game;
	
	
	
	public Window(int width,int height)
	{
		try 
		{
			img=ImageIO.read(new File("img/tetris.png"));
		}catch(IOException e) {
			e.printStackTrace();
		}
		this.setTitle("J-TRICE");
		this.setSize(new Dimension(width, height));
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setIconImage(img);
		this.addKeyListener(new GetKey(this));
		loadContent(toLoad);

		
		this.setVisible(true);
	}
	//chargement du contentPane
	
	
	public void loadContent(int load) {
		
		this.getContentPane().removeAll();
		
		switch(toLoad) {
		  case 1:
		    System.out.println("chargement du menu principal");
		    MainMenu mMenu=new MainMenu(this.getWidth(),this.getHeight());
			for(MenuButton btn:mMenu.getMainMenuBtns()) {
				btn.addActionListener(this);
			}
			
		    this.setContentPane(mMenu);
		    break;
		    
		  case 2:
			    System.out.println("chargement du menu option");
			    MenuOption oMenu=new MenuOption(this.getWidth(),this.getHeight());
			    oMenu.getBtnRetour().addActionListener(this);
			    this.setContentPane(oMenu);
				

			    break;
		
		  case 3:
			    System.out.println("chargement du menu mode");
			    
			    break;
		  
		  case 4:
			  System.out.println("chargement de l'ecran de jeu");
			  this.game=new GameScreen(this.getWidth(),this.getHeight());
			  this.setContentPane(game);

			 
			  break;
		  
		  default:
			    System.out.println("error");
		}
		this.validate();
	}	
	
	
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()) {
		  case "Jouer":
		    System.out.println("clic sur jouer !");
		    this.toLoad=4;
		    this.loadContent(toLoad);
		    break;
		    
		  case "Option":
			    System.out.println("clic sur option !");
			    this.toLoad=2;
			    this.loadContent(toLoad);
			    break;
		  
		  case "Mode de jeu":
			    System.out.println("clic sur mode de jeu !");
			    break;
		  case "Quitter":
			    System.out.println("clic sur quitter !");
			    break;
			    
		  case "Retour":
			    System.out.println("clic sur retour !");
			    this.toLoad=1;
			    this.loadContent(toLoad);
			    break;
	
		  default:
			    System.out.println("error");
		}

	}

	//class interne
	class GetKey implements KeyListener{
		private Window fen;
		public GetKey(Window fen) {
			this.fen=fen;
		}
		
		@Override
		public void keyTyped(KeyEvent e) {
			
		}

		@Override
		public void keyPressed(KeyEvent e) {
//			System.out.println("keyCode : "+e.getKeyCode()+" - keyTxt : "+e.getKeyText(e.getKeyCode()));
			
			
			if(toLoad==4 && fen.game.getMainPan().getGame()) {
				if(e.getKeyCode()==Config.left) fen.game.getMainPan().goLeft();
				if(e.getKeyCode()==Config.right) fen.game.getMainPan().goRight();
				if(e.getKeyCode()==Config.down && fen.game.getMainPan().checkDown()) {
					//ajouter 1xlvl point
					fen.game.getMainPan().getRightPan().setNbPts(fen.game.getMainPan().getRightPan().getNbPts()+(1*fen.game.getMainPan().getLeftPan().getLevel()));
					fen.game.getMainPan().getRightPan().repaint();
					fen.game.getMainPan().goDown();
				}
				if(e.getKeyCode()==Config.rot1) fen.game.getMainPan().rotation(true);
				if(e.getKeyCode()==Config.rot2) fen.game.getMainPan().rotation(false);
			}
			if(toLoad==4) {
				if(e.getKeyCode()==Config.pause) fen.game.getMainPan().pause();
			}

			
		}

		@Override
		public void keyReleased(KeyEvent e) {

		}
	}
	
	
}
