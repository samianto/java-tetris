package javaTetris;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class InfoPan extends JPanel{

	private Image img;
	private int nbPts=0;
	private int best;
	private int[] dataPreview;
	private int[][] preview;
	private Image[] tetrImg=new Image[7];
	
	public InfoPan(int width, int height) {
		System.out.println("chargement des images des tetrinos");	
			try {
				for(int i=0;i<7;i++) {
					String filePath="img/tetrino/tetri"+String.valueOf(i+1)+"elm.png";
					tetrImg[i]=ImageIO.read(new File(filePath));
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
		System.out.println("chargement de l'image de fond du panneau info");	
		try {
			img=ImageIO.read(new File("img/droite.png"));
		}catch(IOException e) {
			e.printStackTrace();
		}
		this.setPreferredSize(new Dimension(width,height));
		
		this.dataPreview=Tetrinos.GenTetrino();
		this.preview=Tetrinos.getTetrino(dataPreview[0],dataPreview[1]);

		
	}

	
	public void paintComponent(Graphics g) {
		g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		g.setFont(new Font("Ink Free", Font.BOLD,35));
		g.setColor(Color.WHITE);
		g.drawString("Next", 55, 90);
		int pX=60;
		int pY=150;
		for(int i=0;i<4;i++) {
			g.drawImage(tetrImg[this.preview[8][0]-1], pX+(this.preview[i][0]*35), pY+(this.preview[i][1]*35), 35, 35,this);
		}
		//affichage des points
		if(nbPts<2) g.drawString("Point", 55, 620);
		else g.drawString("Points", 55, 620);
		g.setFont(new Font("Ink Free", Font.BOLD,20));
		g.drawString(String.valueOf(nbPts), 50, 650);
		g.drawString("Best", 50, 680);
		g.drawString(String.valueOf(best), 50, 700);
	}

	//methodes
	public void addScore(int nbLine, int level) {
		switch(nbLine) {
		case 1 :
			this.nbPts=this.nbPts+(40*level);
			break;
		case 2:
			this.nbPts=this.nbPts+(100*level);
			break;
		case 3 :
			this.nbPts=this.nbPts+(300*level);
			break;
		case 4:
			this.nbPts=this.nbPts+(1200*level);
			break;
		}
		this.repaint();
		
	}
	
	//getteur setteurs
	public int getNbPts() {return this.nbPts;}
	public int getBest() {return this.best;}
	public int[] getDataPreview() {return this.dataPreview;}
	public int[][] getPreview() {return this.preview;}
	
	
	public void setNbPts(int nbPts) {this.nbPts=nbPts;}
	public void setBest(int best) {this.best=best;}
	public void setDataPreview(int[] dataPreview) {this.dataPreview=dataPreview;}
	public void setPreview(int[][] preview) {this.preview=preview;}






}